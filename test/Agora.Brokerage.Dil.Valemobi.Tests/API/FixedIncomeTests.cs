using Newtonsoft.Json;
using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.API {
    public class FixedIncomeTests {

        [SetUp]
        public void Setup () {

        }

        [Test]
        public void TestNothing () {
            Assert.IsTrue (true);
        }

        [Test]
        public void TestFixedIncomeJsonToObject () {

            string json = @"{
                        ""cnpj"" : ""99999999999999"",
                        ""ds_bussiness"" : ""Captação a 100,50%."",
                        ""ds_index"" : ""CDI"",
                        ""dt_maturity_calc"" : ""20180320"",
                        ""id_bond"" : 1234,
                        ""id_instrument"" : 9999,
                        ""nm_index"" : ""CDI"",
                        ""nm_issuer"" : ""Banco Pan"",
                        ""nm_title"" : ""CDB POS"",
                        ""nm_title_type"" : ""CDB"",
                        ""pz_maturity"" : 42,
                        ""pz_maturity_normal_day"" : 63,
                        ""id_tipo_imposto"" : 2,
                        ""qt_available"" : 999999999,
                        ""vl_initial_application"" : 3000.00,
                        ""vl_pu"" : 1000.00,
                        ""rule_tx_operation"" : ""pc_indexer"",
                        ""pc_indexer"" : ""101,00%"",
                        ""pc_tx_pre"" : """",
                        ""url_document"" : ""/data-keynes/rendafixa/document.pdf""
                        }";

            FixedIncome fixedIncome = Build (json);

            //Asserts
            Assert.IsTrue (fixedIncome.Cnpj.Equals (value: "99999999999999"));
            Assert.IsTrue (fixedIncome.Business.Equals (value: "Captação a 100,50%."));
            Assert.IsTrue (fixedIncome.DsIndex.Equals (value: "CDI"));
            Assert.IsTrue (fixedIncome.MaturityCalc.Equals (value: new System.DateTime(2018,03,20)));
            Assert.IsTrue (fixedIncome.Bond.Equals (1234));
            Assert.IsTrue (fixedIncome.Instrument.Equals (9999));
            Assert.IsTrue (fixedIncome.NmIndex.Equals (value: "CDI"));
            Assert.IsTrue (fixedIncome.Issuer.Equals (value: "Banco Pan"));
            Assert.IsTrue (fixedIncome.Title.Equals (value: "CDB POS"));
            Assert.IsTrue (fixedIncome.TitleType.Equals (value: "CDB"));
            Assert.IsTrue (fixedIncome.Maturity.Equals (42));
            Assert.IsTrue (fixedIncome.MaturityNormalDay.Equals (63));
            Assert.IsTrue (fixedIncome.TipoImposto.Equals (2));
            Assert.IsTrue (fixedIncome.Available.Equals (999999999));
            Assert.IsTrue (fixedIncome.InitialApplication.Equals (3000.00m));
            Assert.IsTrue (fixedIncome.Pu.Equals (value: 1000.00m));
            Assert.IsTrue (fixedIncome.RuleTxOperation.Equals (value: "pc_indexer"));
            Assert.IsTrue (fixedIncome.Indexer.Equals (value: "101,00%"));
            Assert.IsTrue (fixedIncome.TxPre.Equals (value: ""));
            Assert.IsTrue (fixedIncome.Document.Equals (value: "/data-keynes/rendafixa/document.pdf"));
        }

        public FixedIncome Build (string json) {

            var jsonSettings = new JsonSerializerSettings ();
            jsonSettings.DateFormatString = "yyyyMMdd";
            FixedIncome fixedIncome = JsonConvert.DeserializeObject<FixedIncome> (json, jsonSettings);
            // todo - map model to instance
            return fixedIncome;
        }

    }
}