using System;
using System.Collections.Generic;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Agora.Brokerage.Dil.Valemobi.Service;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.API.Impl.Stub {
    public class StubValemobiClientTests {
        private Mock<IValemobiService> mockValemobiService;

        private IValemobiClient valemobiClient;
        private FixedIncome dummyFixedIncomeInstance;

        [SetUp]
        public void Setup () {

            ILoggerFactory loggerFactory = new LoggerFactory ();
            this.valemobiClient = new StubValemobiClient (new ValemobiAPIOptions (), loggerFactory);
        }

        [Test]
        public void TestNothing () {
            Assert.IsTrue (true);
        }

        [Test]

        public void getFixedIncomeShelfTest () {

            // make client call
            FixedIncomeShelf fixedIncomeShelf = this.valemobiClient.GetFixedIncomeShelf ();

            Assert.NotNull (fixedIncomeShelf);
            Assert.AreEqual (fixedIncomeShelf.FixedIncome[0].Cnpj,"99999999999999");
            Assert.AreEqual (fixedIncomeShelf.FixedIncome[0].Business,"Captação a 100,50%.");
            Assert.AreEqual (fixedIncomeShelf.FixedIncome[0].Bond,12344);
            Assert.AreEqual (fixedIncomeShelf.FixedIncome[0].MaturityCalc,new DateTime(2018,03,20));
            Assert.AreEqual (fixedIncomeShelf.FixedIncome.Count,1);
        }
    }
}