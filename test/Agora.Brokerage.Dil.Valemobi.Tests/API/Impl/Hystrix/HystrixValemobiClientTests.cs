using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.Service;
using NUnit.Framework;
using Moq;
using Microsoft.Extensions.Logging;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace Agora.Brokerage.Dil.Valemobi.API.Impl.Hystrix
{
    public class HystrixValemobiClientTests
    {
        private Mock<IValemobiService> mockValemobiService;

        private IValemobiClient valemobiClient;
        private FixedIncome dummyFixedIncomeInstance;

        [SetUp]
        public void Setup()
        {
            ILoggerFactory loggerFactory = new LoggerFactory();

            mockValemobiService = new Mock<IValemobiService>();
            this.valemobiClient = new HystrixValemobiClient(new ValemobiAPIOptions(), mockValemobiService.Object, loggerFactory);

            dummyFixedIncomeInstance = new FixedIncome
            {
                Available = 100,
                Bond = 200,
                Business = "Business",
                Cnpj = "Cnpj",
                Document = "Document",
                DsIndex = "DsIndex",
                Indexer = "Indexer",
                InitialApplication = 1.00M,
                Maturity = 300,
                MaturityCalc = DateTime.Now,
                MaturityNormalDay = 400,
                NmIndex = "NmIndex",
                Pu = 2.00M,
                RuleTxOperation = "RuleTxOperation",
                TipoImposto = 500,
                Title = "Title",
                TitleType = "TitleType",
                TxPre = "TxPre"
            };
        }

        [Test]
        public void GetFixedIncomeShelfHappyPathTest()
        {
            List<FixedIncome> fixedIncomes = new List<FixedIncome>();

            fixedIncomes.Add( dummyFixedIncomeInstance);
            fixedIncomes.Add( dummyFixedIncomeInstance);
            fixedIncomes.Add( dummyFixedIncomeInstance);
            fixedIncomes.Add( dummyFixedIncomeInstance);
            fixedIncomes.Add( dummyFixedIncomeInstance);

            FixedIncomeShelf getFixedIncomeShelfReturn = new FixedIncomeShelf{
                FixedIncome = fixedIncomes,
                Success = true,
                ErrorCode = 0,
                ErrorMessage = null
            };


            mockValemobiService.Setup(m => m.GetFixedIncomeShelf()).Returns(getFixedIncomeShelfReturn);

            // make client call
            FixedIncomeShelf fixedIncomeShelf = this.valemobiClient.GetFixedIncomeShelf();

            Assert.NotNull( fixedIncomeShelf);
            Assert.AreEqual(getFixedIncomeShelfReturn, fixedIncomeShelf );
            Assert.AreEqual(getFixedIncomeShelfReturn.FixedIncome.Count, fixedIncomeShelf.FixedIncome.Count );
        }
    }
}