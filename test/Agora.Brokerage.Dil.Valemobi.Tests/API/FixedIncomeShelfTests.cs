using Agora.Brokerage.Dil.Valemobi.Service;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.API {
    public class FixedIncomeShelfTests {

        [SetUp]
        public void Setup () {

        }

        [Test]
        public void TestNothing () {
            Assert.IsTrue (true);
        }

        [Test]
        public void TestFixedIncomeShelfJsonToObject () {

            string json = @"{
                                ""success"": false,
                                ""errorcode"": 3,
                                ""errormessage"": ""Usu\u00e1rio ou senha incorreto""
                            }";

            FixedIncomeShelf fixedIncomeShelf = new FixedIncomeShelfBuilder(json).Build();

            //Asserts
            Assert.IsTrue (fixedIncomeShelf.Success.Equals (false));
            Assert.IsTrue (fixedIncomeShelf.ErrorCode.Equals(3));
            Assert.IsTrue (fixedIncomeShelf.ErrorMessage.Equals (value: "Usu\u00e1rio ou senha incorreto"));
        }

        [Test]
        public void TestFixedIncomeShelfWithError()
        {

        }
    }
}