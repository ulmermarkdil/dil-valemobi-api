using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.API {
    public class ValemobiExceptionTests {

        private ValemobiException valemobiException;
        [SetUp]
        public void Setup () {

        }

        [Test]
        public void TestNothing () {
            Assert.IsTrue (true);
        }

        [Test]
        public void TestConstructor () {

            int expectedInt = 100;
            string expectedString = "Error!!";
            valemobiException = new ValemobiException (expectedInt, expectedString);

            Assert.IsTrue (valemobiException.ErrorCode.Equals (expectedInt));
            Assert.IsTrue (valemobiException.ErrorMessage.Equals (expectedString));
        }

    }
}