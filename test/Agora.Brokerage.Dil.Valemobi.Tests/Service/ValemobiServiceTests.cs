using System;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.Service {
    public class ValemobiServiceTests {
        private ValemobiService service;
        IOptions<ValemobiAPIOptions> options;
        ValemobiAPIOptions vOptions;
        ILoggerFactory loggerFactory;

        [SetUp]
        public void Setup () {

            loggerFactory = new LoggerFactory ();
            vOptions = new ValemobiAPIOptions () {
                Url = "http://localhost:8888",
                Enabled = true,
                User = "theUser",
                Password = "thePassword"
            };

            options = Options.Create (vOptions);
        }

        [Test]
        public void TestNothing () {
            Assert.IsTrue (true);
        }

        [Test]
        public void TestAggregateExceptionWhileGetFixedIncomeShelf() {

            service = new ValemobiService (options, loggerFactory);

            Assert.That (() => service.GetFixedIncomeShelf (),
                Throws.TypeOf<AggregateException> ());
        }

    }
}