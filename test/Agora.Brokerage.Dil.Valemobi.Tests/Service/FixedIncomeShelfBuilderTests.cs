using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.Service;
using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.Service
{
    public class FixedIncomeShelfBuilderTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void BuildUsingSingleFixedIncomeSuccessResponse()
        {
            string json = @"{
                        ""success"": true,
                        ""response"":
                            [
                                {
                                    ""cnpj"" : ""99999999999999"",
                                    ""ds_bussiness"" : ""Captação a 100,50%."",
                                    ""dt_maturity_calc"" : ""20180320"",
                                    ""id_bond"" : 12344
                                }
                            ]

                        }";

            FixedIncomeShelfBuilder builder = new FixedIncomeShelfBuilder(json);
            Assert.IsNotNull(builder);

            FixedIncomeShelf fixedIncomeShelf = builder.Build();

            Assert.IsNotNull(fixedIncomeShelf);
            Assert.IsNotNull(fixedIncomeShelf.FixedIncome);
            Assert.IsNotEmpty(fixedIncomeShelf.FixedIncome);
            Assert.AreEqual(1, fixedIncomeShelf.FixedIncome.Count);
            Assert.IsTrue(fixedIncomeShelf.Success);

        }

        [Test]
        public void BuildUsingFailedResponse()
        {
            string json = @"{
                        ""success"": false,
                        ""errorcode"": 500,
                        ""errormessage"": ""An error occurred""
                        }";

            FixedIncomeShelfBuilder builder = new FixedIncomeShelfBuilder(json);
            Assert.IsNotNull(builder);

            FixedIncomeShelf fixedIncomeShelf = builder.Build();

            Assert.IsNotNull(fixedIncomeShelf);
            Assert.IsFalse(fixedIncomeShelf.Success);
            Assert.AreEqual(500, fixedIncomeShelf.ErrorCode);
            Assert.AreEqual("An error occurred", fixedIncomeShelf.ErrorMessage);

        }


    }
}