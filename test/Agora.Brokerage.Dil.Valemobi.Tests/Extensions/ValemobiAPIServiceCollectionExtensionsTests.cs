using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.API.Impl.Hystrix;
using Agora.Brokerage.Dil.Valemobi.API.Impl.Stub;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Agora.Brokerage.Dil.Valemobi.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.Extensions
{
    [TestFixture]
    public class ValemobiAPIServiceCollectionExtensionsTests
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EnabledConfigTest()
        {

            // Setup onfig
            var appsettings = @"
            {
                'dil-valemobi-api': {
                    'url': 'http://localhost:8888',
                    'enabled': true,
                    'user': 'theUser',
                    'password': 'thePassword'
                }
            }";

            var config = BuildConfiguration( appsettings);


            IServiceCollection services = new ServiceCollection();
            services.AddOptions();
            services.AddLogging();

            // add valemobi api to container
            services.AddValemobiAPI(config);

            // build container
            var serviceProvider = services.BuildServiceProvider();

            //assert container is properly configured
            var valemobiService = serviceProvider.GetService<IValemobiService>();
            var valemobiAPIClient = serviceProvider.GetService<IValemobiClient>();
            var options = serviceProvider.GetService<ValemobiAPIOptions>();

            Assert.NotNull(valemobiService);
            Assert.NotNull(valemobiAPIClient);
            Assert.NotNull(options);
            Assert.AreEqual("http://localhost:8888", options.Url);
            Assert.IsTrue(options.Enabled);
            Assert.AreEqual("theUser", options.User);
            Assert.AreEqual("thePassword", options.Password);

            Assert.AreEqual(valemobiAPIClient.GetType(), typeof(HystrixValemobiClient));


        }

        [Test]
        public void DisabledConfigTest()
        {

            // Setup onfig
            var appsettings = @"
            {
                'dil-valemobi-api': {
                    'url': 'http://localhost:8888',
                    'enabled': false,
                    'user': 'theUser',
                    'password': 'thePassword'
                }
            }";

            var config = BuildConfiguration( appsettings);


            IServiceCollection services = new ServiceCollection();
            services.AddOptions();
            services.AddLogging();

            // add valemobi api to container
            services.AddValemobiAPI(config);

            // build container
            var serviceProvider = services.BuildServiceProvider();

            //assert container is properly configured
            var valemobiService = serviceProvider.GetService<IValemobiService>();
            var valemobiAPIClient = serviceProvider.GetService<IValemobiClient>();
            var options = serviceProvider.GetService<ValemobiAPIOptions>();

            Assert.NotNull(valemobiService);
            Assert.NotNull(valemobiAPIClient);
            Assert.NotNull(options);
            Assert.AreEqual("http://localhost:8888", options.Url);
            Assert.IsFalse(options.Enabled);
            Assert.AreEqual("theUser", options.User);
            Assert.AreEqual("thePassword", options.Password);

            Assert.AreEqual(valemobiAPIClient.GetType(), typeof(StubValemobiClient));


        }

        [Test]
        public void DefaultConfigTest()
        {

            // Setup onfig
            var appsettings = @"
            {
            }";

            var config = BuildConfiguration( appsettings);

            IServiceCollection services = new ServiceCollection();
            services.AddOptions();
            services.AddLogging();

            // add valemobi api to container
            services.AddValemobiAPI(config);

            // build container
            var serviceProvider = services.BuildServiceProvider();

            //assert container is properly configured
            var valemobiService = serviceProvider.GetService<IValemobiService>();
            var valemobiAPIClient = serviceProvider.GetService<IValemobiClient>();
            var options = serviceProvider.GetService<ValemobiAPIOptions>();

            Assert.NotNull(valemobiService);
            Assert.NotNull(valemobiAPIClient);
            Assert.NotNull(options);
            Assert.AreEqual("http://localhost:8080", options.Url);
            Assert.IsTrue(options.Enabled);
            Assert.IsNull(options.User);
            Assert.IsNull(options.Password);

            Assert.AreEqual(valemobiAPIClient.GetType(), typeof(HystrixValemobiClient));


        }

        static IConfigurationRoot BuildConfiguration(string appsettings)
        {
            var path = CreateTempFile(appsettings);
            string directory = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(directory);

            configurationBuilder.AddJsonFile(fileName);
            return configurationBuilder.Build();
        }


        static string CreateTempFile(string contents)
        {
            var tempFile = Path.GetTempFileName();
            File.WriteAllText(tempFile, contents);
            return tempFile;
        }
    }

}