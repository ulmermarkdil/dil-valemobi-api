using NUnit.Framework;

namespace Agora.Brokerage.Dil.Valemobi.Configuration {
    public class ValemobiAPIOptionsTests {
        private ValemobiAPIOptions valemobiAPIOptions;
        [SetUp]
        public void Setup () {
            valemobiAPIOptions = new ValemobiAPIOptions ();
        }

        [Test]
        public void TestNothing () {
            Assert.IsTrue (true);
        }

        [Test]
        public void TestDefaultProperties () {

            string actual = valemobiAPIOptions.Url;
            string expected = "http://localhost:8080";
            Assert.AreEqual (expected, actual);
        }

  [Test]
        public void TestUrlGetSetProperty () {

            string expected = "https://Valemobi.com/";
            valemobiAPIOptions.Url = expected;

            string actual = valemobiAPIOptions.Url;            
            Assert.AreEqual (expected, actual);
        }

        [Test]
        public void TestUserGetSetProperty () {

            string expected = "valemobi";
            valemobiAPIOptions.User = expected;

            string actual = valemobiAPIOptions.User;            
            Assert.AreEqual (expected, actual);
        }
        
        [Test]
        public void TestPasswordGetSetProperty () {

            string expected = "valemobiPwd";
            valemobiAPIOptions.Password = expected;

            string actual = valemobiAPIOptions.Password;            
            Assert.AreEqual (expected, actual);
        }

         [Test]
        public void TestEnabledGetSetProperty () {

            bool expected = false;
            valemobiAPIOptions.Enabled = expected;

            bool actual = valemobiAPIOptions.Enabled;            
            Assert.AreEqual (expected, actual);
        }

    }
}