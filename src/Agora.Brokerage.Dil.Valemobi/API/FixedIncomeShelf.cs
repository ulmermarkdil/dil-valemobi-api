using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Agora.Brokerage.Dil.Valemobi.API
{
    public class  FixedIncomeShelf
    {
        [JsonProperty("response")]
        public List<FixedIncome> FixedIncome;

        [JsonProperty("success")]
        public bool Success;

        [JsonProperty("errorcode")]
        public int  ErrorCode { get; set;}

        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set;}
    }
}