using System.Collections.Generic;
using System.Threading.Tasks;

namespace Agora.Brokerage.Dil.Valemobi.API
{
    public interface IValemobiClient
    {
        FixedIncomeShelf GetFixedIncomeShelf();
    }
}