using System;

namespace Agora.Brokerage.Dil.Valemobi.API
{
    public class ValemobiException : Exception
    {
        private readonly int errorCode;
        private readonly string errorMessage;

        public ValemobiException(int errorCode, string errorMessage)
        {
            this.errorMessage = errorMessage;
            this.errorCode = errorCode;
        }

        public int ErrorCode => errorCode;

        public string ErrorMessage => errorMessage;
    }
}