using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Agora.Brokerage.Dil.Valemobi.Service;
using Microsoft.Extensions.Logging;

namespace Agora.Brokerage.Dil.Valemobi.API.Impl.Stub
{
    public class StubValemobiClient: IValemobiClient
    {

        private readonly  ValemobiAPIOptions options;
        private readonly ILogger logger;

        public StubValemobiClient( ValemobiAPIOptions options, ILoggerFactory loggerFactory){

            if ( options == null) throw new ArgumentNullException("options required.");
            if ( loggerFactory == null) throw new ArgumentNullException("loggerFactory required.");

            this.options = options;
            logger = loggerFactory.CreateLogger<StubValemobiClient>();
        }
        
        public FixedIncomeShelf GetFixedIncomeShelf()
        {
            logger.LogWarning("Stub implementation executing");

            string json = @"{
                        ""success"": true,
                        ""response"":
                            [
                                {
                                    ""cnpj"" : ""99999999999999"",
                                    ""ds_bussiness"" : ""Captação a 100,50%."",
                                    ""dt_maturity_calc"" : ""20180320"",
                                    ""id_bond"" : 12344
                                }
                            ]

                        }";

            FixedIncomeShelfBuilder builder = new FixedIncomeShelfBuilder( json);

            return builder.Build();
        }

    }
}