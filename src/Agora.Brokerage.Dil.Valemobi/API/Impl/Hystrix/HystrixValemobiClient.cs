using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.API.Impl.Hystrix.Commands;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Agora.Brokerage.Dil.Valemobi.Service;
using Microsoft.Extensions.Logging;

namespace Agora.Brokerage.Dil.Valemobi.API.Impl.Hystrix
{
    public class HystrixValemobiClient: IValemobiClient
    {
        private readonly ILogger<HystrixValemobiClient> logger;
        private readonly IValemobiService service;
        private readonly ILoggerFactory  loggerFactory;

        private readonly ValemobiAPIOptions options;

        public HystrixValemobiClient( ValemobiAPIOptions options, IValemobiService service, ILoggerFactory loggerFactory){

            if ( options == null) throw new ArgumentNullException("options required.");
            if ( loggerFactory == null) throw new ArgumentNullException("loggerFactory required.");
            if ( loggerFactory == null) throw new ArgumentNullException("service required.");
            
            this.loggerFactory = loggerFactory;
            this.logger = loggerFactory.CreateLogger<HystrixValemobiClient>();
            this.service = service;
            this.options = options;
        }

        public FixedIncomeShelf GetFixedIncomeShelf(){
            logger.LogInformation("GetFixedIncomeShelf");
            GetFixedIncomeShelfCommand cmd = new GetFixedIncomeShelfCommand(HystrixCommandNames.FIXED_INCOME_SHELF_COMMAND, service,  loggerFactory);
            FixedIncomeShelf fixedIncomeShelf = cmd.GetFixedIncomeShelf();
            return fixedIncomeShelf;
        }
    }
}