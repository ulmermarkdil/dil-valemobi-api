using System.Collections.Generic;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Agora.Brokerage.Dil.Valemobi.Service;
using Microsoft.Extensions.Logging;
using Steeltoe.CircuitBreaker.Hystrix;

namespace Agora.Brokerage.Dil.Valemobi.API.Impl.Hystrix.Commands
{
    public class GetFixedIncomeShelfCommand : HystrixCommand<FixedIncomeShelf>
    {
        private string name;
        private readonly ILogger logger;
        private readonly IValemobiService service;

        public GetFixedIncomeShelfCommand(string name, IValemobiService service, ILoggerFactory loggerFactory)
            : base(HystrixCommandGroupKeyDefault.AsKey(name))
        {
            this.name = name;
            this.service = service;
            this.logger = loggerFactory.CreateLogger<GetFixedIncomeShelfCommand>();
        }

        public FixedIncomeShelf GetFixedIncomeShelf()
        {
            logger.LogTrace("GetFixedIncomeShelf !!");
            return Execute();
        }

        protected override FixedIncomeShelf Run()
        {
            logger.LogTrace("Run !!");
            return service.GetFixedIncomeShelf();
        }

        protected override FixedIncomeShelf RunFallback()
        {
            logger.LogTrace("RunFallback !!");
            throw new System.NotImplementedException();
        }

    }
}
       


