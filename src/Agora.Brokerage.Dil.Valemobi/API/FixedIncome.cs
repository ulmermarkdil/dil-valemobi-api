using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Agora.Brokerage.Dil.Valemobi.API
{
    public class FixedIncome
    {
        [JsonProperty("cnpj")]
        public string Cnpj { get; set; }

        [JsonProperty("ds_bussiness")]
        public string Business { get; set; }

        [JsonProperty("ds_index")]
        public string DsIndex { get; set; }

        [JsonProperty("dt_maturity_calc")]
        public DateTime MaturityCalc { get; set; }

        [JsonProperty("id_bond")]
        public int Bond { get; set; }

        [JsonProperty("id_instrument")]
        public int Instrument { get; set; }

        [JsonProperty("nm_index")]
        public string NmIndex { get; set; }

        [JsonProperty("nm_issuer")]
        public string Issuer { get; set; }

        [JsonProperty("nm_title")]
        public string Title { get; set; }

        [JsonProperty("nm_title_type")]
        public string TitleType { get; set; }

        [JsonProperty("pz_maturity")]
        public int Maturity { get; set; }

        [JsonProperty("pz_maturity_normal_day")]
        public int MaturityNormalDay { get; set; }

        [JsonProperty("id_tipo_imposto")]
        public int TipoImposto { get; set; }

        [JsonProperty("qt_available")]
        public int Available { get; set; }

        [JsonProperty("vl_initial_application")]
        public decimal InitialApplication { get; set; }

        [JsonProperty("vl_pu")]
        public decimal Pu { get; set; }

        [JsonProperty("rule_tx_operation")]
        public string RuleTxOperation { get; set; }

        [JsonProperty("pc_indexer")]
        public string Indexer { get; set; }

        [JsonProperty("pc_tx_pre")]
        public string TxPre { get; set; }

        [JsonProperty("url_document")]
        public string Document { get; set; }

    }
}