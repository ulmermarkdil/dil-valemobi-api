using System.Threading.Tasks;
using Agora.Brokerage.Dil.Valemobi.API;

namespace Agora.Brokerage.Dil.Valemobi.Service
{
    public interface IValemobiService
    {
         FixedIncomeShelf GetFixedIncomeShelf();
    }
}