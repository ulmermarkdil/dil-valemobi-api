using Agora.Brokerage.Dil.Valemobi.API;
using Newtonsoft.Json;

namespace Agora.Brokerage.Dil.Valemobi.Service
{
    public class FixedIncomeShelfBuilder
    {
        private readonly string json;
        public FixedIncomeShelfBuilder( string json){
            this.json = json;
        }

        public FixedIncomeShelf Build(){

            var jsonSettings = new JsonSerializerSettings();
            jsonSettings.DateFormatString = "yyyyMMdd"; 
            FixedIncomeShelf shelf = JsonConvert.DeserializeObject<FixedIncomeShelf>(this.json, jsonSettings);
            // todo - map model to instance
            return shelf;
        }
    }
}