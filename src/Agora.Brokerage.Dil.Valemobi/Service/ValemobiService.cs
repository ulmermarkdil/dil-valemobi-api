using System;
using System.Net.Http;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Steeltoe.Common.Discovery;

namespace Agora.Brokerage.Dil.Valemobi.Service
{
    public class ValemobiService: IValemobiService
    {
        private readonly ValemobiAPIOptions options;
        private readonly ILogger logger;
        private readonly HttpClientHandler handler;


        public ValemobiService(IOptions<ValemobiAPIOptions> options, ILoggerFactory loggerFactory)
        {
            this.options = options.Value;
            this.logger = loggerFactory.CreateLogger<ValemobiService>();
            this.handler = new HttpClientHandler();

        }
        public ValemobiService( ILoggerFactory loggerFactory)
        {
            this.options = new ValemobiAPIOptions();
            this.logger = loggerFactory.CreateLogger<ValemobiService>();
            this.handler = new HttpClientHandler();

        }

        public FixedIncomeShelf GetFixedIncomeShelf()
        {
            logger.LogInformation("GetFixedIncomeShelf");

            using (var client = new HttpClient(handler)) // possibly instantiate in constructor,  if thread safe and doesnt leak memory
            {
                HttpResponseMessage response = client.GetAsync($"{options.Url}/api/corretora/trusted/v1/renda-fixa/bond-list").Result;
                if (response.IsSuccessStatusCode)
                {
                    FixedIncomeShelf fixedIncomeShelf = new FixedIncomeShelfBuilder(response.Content.ReadAsStringAsync().Result).Build();
                    logger.LogDebug($"{fixedIncomeShelf}");

                    if (fixedIncomeShelf.Success){
                        return fixedIncomeShelf;
                    }else{
                        throw new ValemobiException( fixedIncomeShelf.ErrorCode, fixedIncomeShelf.ErrorMessage);
                    }
                }
                else
                {
                    throw new HttpRequestException( $"Error invoking Valemobi API.  Status Code {response.StatusCode} Reason:{response.ReasonPhrase}");
                }
            }
        }
    }
}