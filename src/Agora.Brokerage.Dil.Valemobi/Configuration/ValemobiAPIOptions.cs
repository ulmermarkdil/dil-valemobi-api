namespace Agora.Brokerage.Dil.Valemobi.Configuration
{
    public class ValemobiAPIOptions
    {
        public const  string OPTIONS_PREFIX = "dil-valemobi-api";

        public ValemobiAPIOptions()
        {
        }

        public string Url {get;set;} = "http://localhost:8080";

        public string User {get; set;}
        public string Password {get; set;}
        public bool Enabled  {get; set;} = true;

    }
}