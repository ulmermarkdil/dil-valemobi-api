using Agora.Brokerage.Dil.Valemobi.API;
using Agora.Brokerage.Dil.Valemobi.API.Impl.Hystrix;
using Agora.Brokerage.Dil.Valemobi.API.Impl.Stub;
using Agora.Brokerage.Dil.Valemobi.Configuration;
using Agora.Brokerage.Dil.Valemobi.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Agora.Brokerage.Dil.Valemobi.Extensions
{
    public static class ValemobiAPIServiceCollectionExtensions
    {

        /// Adding Extension method to extend IServiceCollection to Add Valemobi API
        public static IServiceCollection AddValemobiAPI(this IServiceCollection services, IConfiguration config)
        {

            IConfigurationSection section = config.GetSection(ValemobiAPIOptions.OPTIONS_PREFIX);

            ValemobiAPIOptions options = new ValemobiAPIOptions();
            section.Bind(options);

            services.AddSingleton<ValemobiAPIOptions>( options);

            services.AddSingleton<IValemobiService, ValemobiService>();

            if (options.Enabled){
                services.AddSingleton<IValemobiClient, HystrixValemobiClient>();
            }
            else{
                services.AddSingleton<IValemobiClient, StubValemobiClient>();
            }

            return services;

        }
    }
}